---
sidebar_position: 1
slug: /
---

# Introduction

## Welcome

Welcome to rtDm, the independent Drupal documentation site. We hope much of what you find here to be self-explanatory,
but you may want to check out the [about](about.md) page to learn more about our motivations and philosophy.

This site is sourced in part from the official Drupal and Symfony documentation but also contains a significant
amount of original content. If you find a mistake, have an idea or want to contribute, don't hesitate to get involved.
Even an issue explaining a problem you had with a specific topic is helpful, even if you can't contribute new
documentation text yourself.

### Intended audience

Initially, rtDm's intended audience is developers and technical leaders (CTOs, VPs of Technology, etc.) This is not to
exclude others but rather helps focus initial work on improving the technical quality and accuracy of available Drupal
docs. Specifically, this site seeks to create a hard break from Drupal versions 7 and 9+, which share some conventions
but are very different under the hood. Due to the long service life of Drupal 7 (it went GA in
[2011](https://git.drupalcode.org/project/drupal/-/tree/7.0)) much of the documentation around the web is
ambiguous as to its target major version.

Documentation on non-technical/development topics is welcome in merge requests. Help us port and improve Drupal's
existing documentation!

### Contributing

Merge requests are welcome. Sign up for a free GitLab.com account and
[fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the repository, or click any "Edit this
page" link throughout the existing documentation. A read of the
[Docusaurus v2 docs](https://docusaurus.io/docs/docs-introduction) will also help; the site is basically a vanilla
install, with no blog.

#### Local development

The repository contains a `docker-compose.yml` file which should allow you to run rtDm locally with little to no bespoke
tooling; just `docker-compose up` and find the site at `http://localhost:3000`.
