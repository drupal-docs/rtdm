---
sidebar_position: 3
---

# Mentoring

## Synchronous PHP and Drupal Mentoring

Witnessing a marked increase in "where do I start" queries on
[Drupal Slack](/docs/basics/tooling#chat), let's try a new way of helping people
along their Drupal and PHP journey. On a yet-to-be-decided schedule, we'll host
an informal, synchronous video chat where Drupal users can ask questions, seek
advice and meet others.

### Guidelines for informal mentoring meetups

* This is not an initiative of the Drupal mentors group or the Drupal
  Association.
* Participation is at the discretion of the organizer (the rtDm project lead).
* Everyone, regardless of skill level or experience, is welcome.
* The meetup is conducted in English. Your English need not be expert-level.
* Participants are expected to have their video turned on throughout the
  call and are expected to introduce themselves. No pure lurking.
* The intent of the meetup is to obtain general guidance, advice, and to
  workshop general solutions to problems. Bring your questions and issues for
  discussion. However, the group will not fix your problems for you.
* These guidelines will evolve over time if these events prove helpful and
  become recurring.

### Schedule and RSVP

* February 21, 2023,
  [15:00 UTC](https://www.timeanddate.com/worldclock/converter.html?iso=20230221T150000&p1=3754&p2=136&p3=771).
  RSVP [here](https://docs.google.com/forms/d/e/1FAIpQLSfdv4_HIKrCHzXhpcGlFOsiKUqXIc4am0HTdhCTqcMx4sf-Ow/viewform?usp=sf_link)
  to receive a calendar invite.
