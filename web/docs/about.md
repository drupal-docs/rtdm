---
sidebar_position: 2
---

# About rtDm

## Why an independent documentation site?

[Drupal](https://drupal.org) is awesome. Its [community](https://www.drupal.org/community) is one of the strongest in
free and open-source software. Its [documentation](https://www.drupal.org/documentation), however, is hamstrung in large
part due to out-of-date tooling and a lack of meaningful versioning. This is not at all to disparage the hard work of
the many contributors to the official documentation.

Forking the documentation into a more modern format (Docusaurus) and a code repository allows the existing documentation
to be repackaged in a more accessible way for both readers and contributors.

## Opinionated content

In addition to considerations around tooling, it's worth also noting that documentation can be _opinionated_. This is a
feature of rtDm vs. the official documentation. Not everyone will always agree on the "right" way to do Drupal (or any
software, for that matter.) An independent, unofficial docs site is free to espouse more opinionated perspectives on
what many would call "best practices," avoiding the inevitable politics of official documentation that must
respect multiple constituencies. This of course implies different contribution dynamics than other projects. The rtDm
maintainers reserve the right to reject or amend contributions in accordance with their preferences.

Put another way, rtDm seeks to be _accurate_ but _opinionated_ while always being _respectful and professional_ in tone.

## License

The documentation is licensed under a [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
license, chosen for compatiblity with the Drupal and Symfony docs, from which we borrow liberally. If you are copying
documentation from other projects, please ensure the license is compatible and add to the `LICENSE.md` file as
appropriate, noting your sources.

## Leadership

Currently the project is governed by a single
[founder-leader](https://www.redhat.com/en/resources/guide-to-open-source-project-governance-models-overview),
[Brad Jones](https://gitlab.com/bradjones1).
