---
sidebar_position: 1
---

# About Drupal

## What is Drupal?

Drupal is a free and open-source content-management framework that can be tailored and customized to build simple
websites or complex web applications. Drupal grows as you grow with thousands of free modules and themes that will help
you attract the web audience you need to deliver your message, grow brand awareness, and build your community.

Drupal is accessible and multilingual, and flexible by design.

## Technology Overview

Drupal is written in [PHP](https://www.php.net) and uses many components from
[Symfony](https://symfony.com/doc/current/index.html), however it is not strictly speaking a Symfony app. (We explore
later the ways in which Drupal [diverges from modern Symfony](drupal-and-symfony#where-drupal-diverges-from-symfony).)
In versions prior to 8.0, Drupal maintained an entirely bespoke codebase from top to bottom. From version 8.0, Drupal
leverages dependency management via [Composer](https://getcomposer.org). The inclusion of external libraries allows core
maintainers to reduce the amount of custom code in areas such as low-level request handling and security.

### Technical requirements

Drupal may be hosted on a wide range of environments and carries with it a modest list of runtime dependencies.

| Requirement | Version Constraints |
| --- | --- |
| HTTP server | Nginx `>= 0.7` or Apache `>= 2.4.7` |
| PHP | `>= 7.3` |
| Database | _One of:_<br />MySQL or Percona `>= 5.7.8`<br />MariaDB `>= 10.3.7`<br />SQLite ([compiled into PHP](https://www.php.net/manual/en/sqlite3.installation.php)) `>= 3.26`<br />PostgreSQL `>= 10` with the `pg_trgm` extension. |

## Community and Governance

Drupal, like many other open-source software projects, started as a hobbyist endeavour and grew to become a large,
well-organized project with its own [governance](https://www.drupal.org/contribute/core/maintainers)
structure, user base and technical culture. The project is backed by a nonprofit, the
[Drupal Association](https://association.drupal.org), which is primarily funded through convention revenues and
memberships and pays for the project's tooling at `drupal.org`.

### Drupal culture
