# Drupal.org & Tooling

## Drupal.org

[Drupal.org](https://drupal.org) is unique among open-source projects in that it hosts a home-grown issue tracker,
integrated module and theme directory and user management/version control, all within a walled garden.

There is an active initiative to move much of this functionality from `drupal.org` to a self-hosted GitLab instance.
However, the current model speaks to Drupal's culture as much as it does anything about development. By hosting core and
contributed projects inside a single platform Drupal is able to provide a more unified experience than other software
ecosystems like WordPress. Drupal's infrastructure can therefore offer features like centralized coordination of
security vulnerabilities, publish an official Composer repository, and reward individuals and corporations with credit
for work on the project.

Even after reducing custom tooling by moving many functions to GitLab, Drupal.org will remain the hub of development and
community activity for the ecosystem writ large.

## Version Control

Development of Drupal core and contributed modules, themes, profiles, etc. is _primarily_ performed on a hosted version
of GitLab tightly integrated with the Drupal.org issue tracker. Users authenticate to the GitLab instance via single
sign-on from drupal.org. Permissions on repositories are mapped to "maintainer" roles on the corresponding drupal.org
project.

:::caution

Do not confuse Drupal's customized GitLab instance with GitLab.com or even your own GitLab CE or EE installation. While
an initiative is underway to expand its enabled features, it is currently "just" a git remote, code browser and merge
request manager. Do not yet depend on other functionality.

:::

Users may propose code changes through a merge request process, but repository forks are not made into a user's
namespace. Rather, a button on the drupal.org issue will create an "issue fork" akin to an entirely new GitLab project.
This fork allows any other user to collaborate on the fork, regardless of their permissions on the Drupal.org project.
This workflow may be confusing to users used to a more "traditional" repository forking process, even on GitLab.org.

:::tip

Some projects in the Drupal ecosystem are not waiting for Drupal.org to fully switch its collaboration tools to GitLab.
[DrupalSpoons](https://weitzman.github.io/blog/drupalspoons-announcement) is one example of developers using "vanilla"
GitLab.com to demonstrate how easy it should be to transition away from Drupal.org's bespoke systems.

:::

### A note about patch files

Newer software developers (hello bootcamp graduates!) may be unfamiliar with the
[ancient art of patchfiles](https://en.wikipedia.org/wiki/Patch_%28Unix%29#Patches_in_software_development) for
communicating changes to software. While patches remain a completely valid tool (more on using them, later) the advent
of distributed version control means most software libraries are now developed with
[merge requests](https://en.wikipedia.org/wiki/Distributed_version_control#Pull_requests). Drupal started its version
control life using [CVS](https://en.wikipedia.org/wiki/Concurrent_Versions_System), and converted to Git in 2011. Even
after that conversion, there was no tooling for merge requests so Drupal developers have maintained a tradition of
posting patch files containing a [unified diff](https://en.wikipedia.org/wiki/Diff#Unified_format) of the changes.

When it is helpful to show changes from a previous version of a diff, the
[`interdiff` command](https://www.linux.org/docs/man1/interdiff.html) can generate a diff of the two patches. This can
get tedious and difficult to follow, which is another reason to prefer merge requests.

:::info

You will continue to encounter patch files in issue queues as not everyone uses the MR option yet. For purposes of this
documentation, we'll show you how to apply patches from issue queues as well as generate diff files for merge requests,
but you should always prefer an MR workflow when _proposing_ changes on Drupal.org.

:::

:::note

We're using the terms "diff" and "patch" interchangeably here, but there are
subtle [differences](https://opensource.com/article/18/8/diffs-patches) in the formatting of files that can be applied
using the [`patch` command](https://www.man7.org/linux/man-pages/man1/patch.1.html) or
[`git apply`](https://git-scm.com/docs/git-apply). In practice, you can safely ignore these differences.
The `cweagans/composer-patches` library
[uses `git apply`](https://github.com/cweagans/composer-patches/issues/172#issuecomment-346464525) when patching
Composer dependencies.

:::

## Chat

## Other common places to collaborate
