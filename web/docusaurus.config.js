// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'rtDm: Read the Drupal Manual',
  tagline: 'Independent Drupal Documentation',
  url: 'https://rtdm.info',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.

  presets: [
    [
      '@docusaurus/preset-classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: 'https://gitlab.com/drupal-docs/rtdm/-/edit/master/web',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/drupal-docs/rtdm/-/edit/master/web/blog',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    {
      gtag: {
        trackingID: 'G-PV2YLPXLBW',
        anonymizeIP: true,
      },
      navbar: {
        title: 'rtDm',
        logo: {
          alt: 'rtDm',
          src: 'img/logo.svg',
        },
        items: [
          {
            label: "Docs",
            to: "/docs"
          },
          {
            label: "API Explorer",
            to: "https://api.rtdm.info/9.3.0/"
          },
          {
            href: 'https://gitlab.com/drupal-docs/rtdm',
            label: 'Source',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'About',
                to: '/docs/about',
              },
            ],
          },
          {
            title: 'Contribute',
            items: [
              {
                label: 'GitLab',
                href: 'https://gitlab.com/drupal-docs/rtdm',
              },
            ],
          },
        ],
        copyright: `Copyright © 2021-${new Date().getFullYear()} <a href="https://tech.kinksters.dating">Not Vanilla, Inc.</a> and individual contributors. Licensed <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>.<br />Built with Docusaurus, hosted on GitLab Pages.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }
};

module.exports = config;
