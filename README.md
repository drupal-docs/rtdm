# rtDm (Read the Drupal Manual)

This repository is the source for [rtdm.info](https://rtdm.info), built with [Docusaurus]().

### Copyright and license

This work is copyright 2000-present by the individual contributors to this repository, the
[Drupal.org documentation](https://www.drupal.org/documentation) (CC BY-SA 2.0; from which much of this site is forked
from) and the [Symfony documentation](https://symfony.com/doc/current/index.html) (CC BY-SA 3.0).

The site content is licensed as
[Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/).
